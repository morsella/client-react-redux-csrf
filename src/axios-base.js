import axios from 'axios';

export const instance = axios.create({
    baseURL: 'http://localhost:3030'
});
export const adminInstance = (token) => {
    return axios.create({
            baseURL: 'http://localhost:3030',
            withCredentials: true,
            headers: { Authorization: "Bearer " + token }
    });
};
