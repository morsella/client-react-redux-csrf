import * as actionTypes from '../actions/actionsTypes';
import { updateObject } from '../../shared/utility';
const initState={
    cartItems: [],
    total: null,
    orders: [],
    error: null,
    methods: [],
    loading: false,
    purchased:false,
    cartRemarks: null,
    customerForm: {
        firstName: null,
        lastName: null,
        companyName: null,
        street: null,
        houseNumber: null,
        afix: null,
        postCode: null,
        city: null,
        phoneNumber: null,
        email: null
    }
};
const deleteCartItem = ( state, action ) => {
    return updateObject(state);
};
const addCartItem = (state, action) =>{
    console.log('NEW ITEM', action.item);
    const findItem = state.cartItems.find((i) => {
        const checkItem = i.id === action.item.id && (i.price1 === action.item.price1 || i.price2 === action.item.price2) ?
        i : null;
        return checkItem;
    });
    const newItem = findItem ? updateObject(findItem, action.item) : updateObject( action.item, { id: action.item.id } );
    const updatedCart = {
        ...state.cartItems,
        ...state.cartItems.map(ci => 
         ci.id === newItem.id && (ci.price1 === newItem.price1 || ci.price2 === newItem.price2) ? 
         {...ci, quantity: newItem.quantity} : ci
       )
    };
    console.log('updated', updatedCart);
    return updateObject( state, {
        loading: false,
        purchased: true,
        cartItems: findItem ? Object.values(updatedCart) : state.cartItems.concat( newItem )
    });
};
const updateCart = (state, action) =>{
    return updateObject(state,{
        cartItems: action.cartItems,
        loading: false
    });
};
const clearCart = (state, action) => {
    return updateObject(state, {
        cartItems: [],
        total: null
    });
};
const cartLoading = (state, action) => {
    console.log(state.loading);
    return updateObject(state, {
        loading: true
    });
};
const getMethods = (state, action) =>{
    console.log('methods', action.methods);
    return updateObject(state, {
        methods: action.methods
    });
};
const calculateTotal = (state, action) => {
    console.log('totals', action.total);
    return updateObject(state, {
        total: action.total
    });
};
const cartRemarks = (state, action) => {
    console.log('remarks', action.cartRemarks);
    return updateObject(state, {
        cartRemarks: action.cartRemarks
    });
};
const customerForm = (state, action) => {
    const control = action.controlName;
    const fieldVal = action.customerFormVal;
    const customerFieldUpdated  = {...state.customerForm, [control] : fieldVal};
    return updateObject(state, {
        customerForm: customerFieldUpdated
    });
;}

const reducer = (state = initState, action) => {
    switch (action.type){
        case actionTypes.ADD_CART_ITEM: return addCartItem(state, action);
        case actionTypes.UPDATE_CART: return updateCart(state, action);
        case actionTypes.CLEAR_CART: return clearCart(state, action);
        case actionTypes.CART_LOADING: return cartLoading(state, action);
        case actionTypes.DELETE_CART_ITEM: return deleteCartItem(state, action);
        case actionTypes.GET_METHODS: return getMethods(state, action);
        case actionTypes.CALCULATE_TOTAL: return calculateTotal(state, action);
        case actionTypes.CART_REMARKS: return cartRemarks(state, action);
        case actionTypes.CUSTOMER_FORM: return customerForm(state, action);
        default: return state;
    }
};

export default reducer;