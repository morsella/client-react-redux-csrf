import * as actionTypes from '../actions/actionsTypes';
import { updateObject } from '../../shared/utility';
const initState={
    items: [],
    categories: [],
    itemError: null,
    categoryError: null,
    loading: false
};
const initAddCategory = (state, action) =>{
    return updateObject(state, {
        categoryError: null,
        loading: true
    });
};
const initAddItems = (state, action) =>{
    return updateObject(state, {
        itemError: null,
        loading:true
    });
};
const addItem = (state, action) => {
    const newItem = updateObject( action.item, { id: action.item.id } );
    return updateObject( state, {
        loading: false,
        items: state.items.concat( newItem )
    });
};
const addCategory = (state, action) => {
    const newCategory = updateObject( action.category, { id: action.category.id } );
    return updateObject( state, {
        loading: false,
        categories: state.categories.concat( newCategory )
    });
};
const getCategories = (state, action) => {
    return updateObject(state, {
        categories: action.categories,
        loading: false
    });
};
const getItems = (state, action) =>{
    return updateObject(state, {
        items: action.items,
        loading: false
    });
};

const addItemFail = (state, action) =>{
    return updateObject(state, {
        itemError: action.error,
        loading: false
    });
};
const addCategoryFail = (state, action) =>{
    return updateObject(state, {
        categoryError: action.error,
        loading: false
    });
};

const reducer = (state = initState, action) => {
    switch (action.type){
        case actionTypes.ADD_ITEM: return addItem(state, action);
        case actionTypes.ADD_CATEGORY: return addCategory(state, action);
        case actionTypes.GET_CATEGORIES: return getCategories(state, action);
        case actionTypes.GET_ITEMS: return getItems(state, action);
        case actionTypes.ADD_ITEM_FAIL: return addItemFail(state, action);
        case actionTypes.ADD_CATEGORY_FAIL: return addCategoryFail(state, action);
        case actionTypes.INIT_ADD_CATEGORY: return initAddCategory(state, action);
        case actionTypes.INIT_ADD_ITEMS: return initAddItems(state, action);
        // case actionTypes.DELETE_ITEM: return deleteItem(state, action);
        // case actionTypes.UPDATE_ITEM: return updateItem(state, action);
        default: return state;
    }
};

export default reducer;