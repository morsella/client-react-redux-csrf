import * as actionTypes from '../actions/actionsTypes';
import { updateObject } from '../../shared/utility';
const initState={
    csrf: null,
    // sessionToken: null,
    loading: false
};
const setCsrf = (state, action) => {
    return updateObject(state, {
        csrf: action.csrf,
        loading: true
    });
};
// const setSessionToken = (state, action) =>{
//     return updateObject(state, {
//         sessionToken: action.session,
//         loading: true
//     });
// }
const initSession = (state, action) =>{
    return updateObject(state,{
        loading: true
    });
}
const sessionSuccess = (state, action) =>{
    return updateObject(state,{
        loading: false
    });
};
const reducer = (state = initState, action) => {
    switch (action.type){
        case actionTypes.SET_CSRF: return setCsrf(state, action);
        case actionTypes.INIT_SESSION: return initSession(state, action); 
        case actionTypes.SESSION_SUCCESS: return sessionSuccess(state, action);
        // case actionTypes.SET_SESSION_TOKEN: return setSessionToken(state, action);
        default: return state;
    }
};

export default reducer;