import * as actionTypes from './actionsTypes';
import {instance} from '../../axios-base';

export const setCsrf = (csrf) =>{
    return {
        type:actionTypes.SET_CSRF,
        csrf:csrf
    };
};

export const initSession = () =>{
    return {
        type: actionTypes.INIT_SESSION
    };
};
export const sessionSuccess = () =>{
    return {
        type: actionTypes.SESSION_SUCCESS
    };
};

 export const getSession = () =>{
     console.log('start session');
    const session = localStorage.getItem('session-init');
    const headers = session ? {'session': session } : null;

    return dispatch => {
        dispatch(initSession());
        instance.get( '/api/init', {withCredentials:true, headers: headers})
            .then(res => {
                const sessionInitToken = res.data.secure;
                instance.get('/api/secure', {withCredentials: true, headers: {'session': sessionInitToken}})
                .then(res2 => {
                    localStorage.setItem('session-init', res2.data.secure);
                    dispatch(startCookieTimeout(res2.data.expiresIn));
                    dispatch(setCsrf(res2.data.secure));
                    dispatch(sessionSuccess());
                })
            })
            .catch( err => {
                localStorage.removeItem('session-init');
                let error = err.response.data.message;
                console.log('ERROR', error);
            } );
    };
};

const startCookieTimeout = (time) =>{
    return dispatch =>{
        setTimeout(()=>{
            dispatch(getSession());
        }, time * 1000);
    };
};
