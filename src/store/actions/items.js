import {adminInstance, instance} from '../../axios-base';
import * as actionTypes from './actionsTypes';


// sync action
export const getCategories = (categories) => {
    return {
        type: actionTypes.GET_CATEGORIES,
        categories: categories
    };
};
export const getItems = (items) =>{
    return {
        type: actionTypes.GET_ITEMS,
        items: items
    };
};
export const addItem = (item) =>{
    return {
        type: actionTypes.ADD_ITEM,
        item: item
    };
};
export const addCategory = (category) => { 
    return {
        type: actionTypes.ADD_CATEGORY,
        category: category
    };
};

export const addItemFail = (error) =>{
    return {
        type: actionTypes.ADD_ITEM_FAIL,
        error: error
    };
};
export const addCategoryFail = (error) =>{
    return {
        type: actionTypes.ADD_CATEGORY_FAIL,
        error: error
    };
};
export const initCategory = () =>{
    return {
        type: actionTypes.INIT_ADD_CATEGORY
    };
};
export const initItems = () =>{
    return {
        type: actionTypes.INIT_ADD_ITEMS
    };
};
export const fetchCategories = () =>{
    return dispatch => {
        dispatch(initCategory());
        instance.get( '/categories')
            .then( res => {
                const fetchedCategories = [];
                for ( let c in res.data.categories ) {
                    fetchedCategories.push({
                        ...res.data.categories[c]
                    } );
                }
                dispatch(getCategories(fetchedCategories));
            } )
            .catch( err => {
                let error = err.response.data.message;
                console.log('ERROR', error);
                dispatch(addCategoryFail(error));
            });
     };
};
export const fetchItems = () =>{
    return dispatch => {
        dispatch(initItems());
        instance.get('/items')
            .then(res =>{
                const fetchedItems = [];
                for ( let i in res.data.items ) {
                    fetchedItems.push({
                        ...res.data.items[i]
                    } );
                }
                dispatch(getItems(fetchedItems));
            }).catch(err =>{
                let error = err.response.data.message;
                console.log('ERROR', error);
                dispatch(addItemFail(error));
            });
    };
};
export const categoryToAdd = (name) => {
    const category ={
        text: name
    };
    return dispatch => {
        dispatch(initCategory());
        const token = localStorage.getItem('token');
        adminInstance(token).post('/categories', category).then(res => {
            dispatch(addCategory(res.data.category));
        }).catch(err => {
            let error = err.response.data.message;
            console.log('ERROR', error);
            dispatch(addCategoryFail(error));
        })
    };
};
export const itemToAdd = (item) => {
    return dispatch => {
        const token = localStorage.getItem('token');
        dispatch(initItems());
        adminInstance(token).post('/items/', item).then(res =>{
            dispatch(addItem(res.data.item));
        }).catch(err => {
            let error = err.response.data.message;
            console.log('ERROR', error);
            dispatch(addItemFail(error));
        })
    };
};