import * as actionTypes from './actionsTypes';
import {instance} from '../../axios-base';
import { getSession } from './session';
// import {setSessionToken, sessionSuccess} from '../actions/session';

export const addCartItem = (item) => {
    return {
        type: actionTypes.ADD_CART_ITEM,
        item: item
    };
};
export const removeCartItem = (item) => {
    return {
        type: actionTypes.DELETE_CART_ITEM,
        item: item
    };
};
export const updateCart = (cart) =>{
    return {
        type: actionTypes.UPDATE_CART,
        cartItems: cart
    };
};
export const getMethods = (methods) =>{
    return {
        type: actionTypes.GET_METHODS,
        methods: methods
    };
};
export const calculateTotal = (total) => {
    return {
        type: actionTypes.CALCULATE_TOTAL,
        total: total
    };
};
export const cartRemarks = (remarks) => {
    return {
        type: actionTypes.CART_REMARKS,
        cartRemarks: remarks
    };
};
export const customerForm = (customerForm, controlName) => {
    return {
        type: actionTypes.CUSTOMER_FORM,
        customerFormVal: customerForm, 
        controlName: controlName
    };
};
//******************************************************************************** */
export const fetchMethods = () =>{
    return dispatch => {
        instance.get('/cart/methods').then(res=> {
            console.log('res fetch methods', res);
            dispatch(getMethods(res.data.payments));
        }).catch( error => {
            // dispatch( purchaseFail( error ) );
        } );;
    }
};
export const createPayment = (payment) =>{
    return dispatch => {
        instance.post('/cart/payment', payment, {withCredentials: true}).then(res =>{
            console.log('res create payment', res);
            if(res.data.payment._links.checkout){
                
                window.open(res.data.payment._links.checkout.href, "_self");
                console.log('res create payment', res.data.payment);

            }
        }).catch(err => {
            let error = err.response.data.message;
            if( error === 'No valid token'){
                dispatch(getSession());
            };
            console.log(error);
        });
    };
};

export const cartLoading = () =>{
    return {
        type: actionTypes.CART_LOADING
    };
};
export const clearCart = () => {
    localStorage.removeItem('cart-token');
    localStorage.removeItem('cart');
    localStorage.removeItem('cartExpiration');
    return {
        type: actionTypes.CLEAR_CART
    };
};
// const checkCart = () =>{
//     return dispatch => {
//         const cart = localStorage.getItem('cart');
//         const cartSession = localStorage.getItem('cart-token');
//         const cartExp= localStorage.getItem('cartExpiration');
//         if(!cart || !cartSession || !cartExp || cart === 'undefined' || cartSession === 'undefined' || cartExp === 'undefined'){
//         dispatch(clearCart());
//         }
//     };
// };
export const totalCart = () => {
    return dispatch => {
        dispatch(calculateTotal());
    };
};
 export const getCartItems = () =>{
     const cart = JSON.parse(localStorage.getItem('cart')) || [];
     console.log(cart);
     return dispatch => {
        dispatch(updateCart(cart));
    };
    // const cartSession = localStorage.getItem('cart-token');
    // const cartExpiration = new Date(localStorage.getItem('cartExpiration'));
    //     return dispatch => {
    //         dispatch(checkCart());
    //         if( cartSession && cartExpiration > new Date()){
    //         instance.get( '/cart', {withCredentials:true, headers:{'cart-session': cartSession}})
    //         .then( res => {
    //             localStorage.setItem('cart-token', res.data.cartSession);
    //             dispatch(updateCart(res.data.cart));
    //             dispatch(startCartTimeout(res.data.cartExpires));
    //         })
    //         .catch( err => {
    //             let error = err.response.data.message;
    //             console.log('ERROR', error);
    //             dispatch(clearCart());
    //             // dispatch(addCategoryFail(error));
    //         } );
    //     }
    //  };
};
const addToCartObj = (item) =>{
    return dispatch => {
        // dispatch(cartLoading());
        // dispatch(checkCart());
        const cart = JSON.parse(localStorage.getItem('cart')) ||  [];
        const cartItem = cart.find(i => { 
            const findItem = i.id === item.id && (i.price1 === item.price1 || i.price2 === item.price2) ?
            i : null;
            return findItem;
        });
        console.log('item', item);
        console.log('found', cartItem);
        if(cartItem){
            cartItem.quantity += 1;
            console.log('action cart', cart);
            console.log('action id', cartItem.id);
            const newCart = {...cart, ...cartItem.id};
            console.log('newCart', Object.values(newCart));
            localStorage.setItem('cart', JSON.stringify(Object.values(newCart)));
            dispatch(addCartItem(cartItem));
        }else{
            item.quantity = 1;
            cart.push(item);
            console.log('Cart', cart);
            localStorage.setItem('cart', JSON.stringify(cart));
            dispatch(addCartItem(item));
        }
    };
};
// const addToCartItem = (item, csrf) =>{
//     let cart = {
//         _csrf: csrf,
//         item: item
//     };
//     const cartSession = localStorage.getItem('cart-token');
//     const headers = cartSession ? {'cart-session': cartSession } : null;
//     return dispatch => {
//         dispatch(cartLoading());
//         dispatch(checkCart());
//         instance.post( '/cart', cart, {withCredentials: true, headers: headers})
//             .then( res => {
//                 dispatch(addCartItem(item));
//                 dispatch(startCartTimeout(res.data.cartExpires));
//                 const expirationDate = new Date(new Date().getTime() + res.data.cartExpires * 1000);
//                 localStorage.setItem('cart-token', res.data.cartSession);
//                 localStorage.setItem('cart', JSON.stringify(res.data.cart));
//                 localStorage.setItem('cartExpiration', expirationDate);
//             } )
//             .catch( err => {
//                 let error = err.response;
//                 console.log('ERROR', error);
//                 dispatch(clearCart());
//                 // dispatch(addCategoryFail(error));
//             } );
//     };
// };
export const cartInit = (item, csrf) => {
    return dispatch => {
        dispatch(cartLoading());
        // dispatch(checkCart());
        dispatch(addToCartObj(item));
        // const cart = localStorage.getItem('cart');
        // const cartSession = localStorage.getItem('cart-token');
        // const cartExp= localStorage.getItem('cartExpiration');
        // if(!cart || !cartSession || !cartExp || cart === 'undefined' || cartSession === 'undefined' || cartExp === 'undefined'){
        //     dispatch(addToCartItem(item, csrf));
        // }else{
        //     dispatch(addToCartObj(item));
        // }        
    }
};

//  const startCartTimeout = (time) =>{
//     return dispatch =>{
//         setTimeout(()=>{
//             //Model promt
//             dispatch(clearCart());
//             // dispatch(getSession());
//         }, time * 1000);
//     };
// };
// export const fetchOrdersSuccess = ( orders ) => {
//     return {
//         type: actionTypes.FETCH_ORDERS_SUCCESS,
//         orders: orders
//     };
// };

// export const fetchOrdersFail = ( error ) => {
//     return {
//         type: actionTypes.FETCH_ORDERS_FAIL,
//         error: error
//     };
// };

// export const fetchOrdersStart = () => {
//     return {
//         type: actionTypes.FETCH_ORDERS_START
//     };
// };

// export const fetchOrders = (token, userId) => {
//     return dispatch => {
//         dispatch(fetchOrdersStart());
//         const queryParams = '?auth=' + token + '&orderBy="userId"&equalTo="' + userId + '"';
//         axios.get( '/orders.json' + queryParams)
//             .then( res => {
//                 const fetchedOrders = [];
//                 for ( let key in res.data ) {
//                     fetchedOrders.push( {
//                         ...res.data[key],
//                         id: key
//                     } );
//                 }
//                 dispatch(fetchOrdersSuccess(fetchedOrders));
//             } )
//             .catch( err => {
//                 dispatch(fetchOrdersFail(err));
//             } );
//     };
// };