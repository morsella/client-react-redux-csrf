import {adminInstance, instance} from '../../axios-base';
import * as actionTypes from './actionsTypes';

// sync action
export const authStart = () =>{
    return {
        type: actionTypes.AUTH_START
    };
};

export const authSuccess = (token) =>{
    return {
        type: actionTypes.AUTH_SUCCESS,
        idToken: token
    };
};

export const authFail = (error) =>{
    return {
        type: actionTypes.AUTH_FAIL,
        error: error
    };
};
export const clearLocal = () =>{
    localStorage.removeItem('token');
    localStorage.removeItem('expirationDate');
    return {
        type: actionTypes.AUTH_LOGOUT
    };
};


//async action
export const auth = (email, password, csrf) =>{
    return dispatch => {
        dispatch(authStart());
        const authData = {
            email: email,
            password: password,
            _csrf: csrf
        };
        instance.post('/admin/login', authData, {withCredentials: true}).then(res =>{
            const expirationDate = new Date(new Date().getTime() + res.data.expiresIn * 1000);
            localStorage.setItem('token', res.headers['x-token']);
            localStorage.setItem('expirationDate', expirationDate);
            dispatch(authSuccess(res.headers['x-token']));
            dispatch(startAuthTimeout(res.data.expiresIn));
        }).catch(err => {
            let error = err.response.data.message;
            dispatch(clearLocal());
            dispatch(authFail(error));
        })
    };
};
export const removeAuth = () => {
    let data = {
        _csrf: localStorage.getItem('_csrf')
    };
    return dispatch => {
        const token = localStorage.getItem('token');
        adminInstance(token).post('/admin/logout', data)
        .then((res)=> {
            dispatch(clearLocal());
        }).catch(err => {
            let error = err.response.data.message;
            dispatch(clearLocal());
            dispatch(authFail(error));
        })
    };
};
const startAuthTimeout = (expTime) =>{
    return dispatch =>{
        setTimeout(()=>{
            dispatch(clearLocal());
        }, expTime * 1000);
    };
};

export const authCheckState = () => {
    return dispatch => {
        const token = localStorage.getItem('token');
        const expirationDate = new Date(localStorage.getItem('expirationDate'));
        if(token && expirationDate > new Date()){
            dispatch(authSuccess(token));
            dispatch(startAuthTimeout((expirationDate.getTime() - new Date().getTime()) / 1000));
        }
    };
};