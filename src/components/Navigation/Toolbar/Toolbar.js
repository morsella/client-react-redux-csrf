import React from 'react';
import classes from './Toolbar.module.css';
// import Logo from '../../Logo/Logo';
import NavigationItems from '../NavigationItems/NaviagtionItems';
// import MenuToggle from '../SideDrawer/MenuToggle/MenuToggle';
const toolbar = (props) =>(
    <header className={classes.Toolbar}>
        {/* <MenuToggle clicked={props.menuToggleClicked}/> */}
        {/* <Logo /> */}
        <h3 className={classes.phone}> <i className="fas fa-phone"></i> 036 785 40 07</h3>
        <nav><NavigationItems isAuthenticated={props.isAuth}/></nav>
    </header>
);

export default toolbar;