import React from 'react';
import {connect} from 'react-redux';
// import * as actions from '../../../store/actions/index';

import classes from './NavigationItems.module.css';
import NavigationItem from './NavigationItem/NavigationItem';

const NavigationItems = (props) => {
    return (
        <ul className={classes.NavigationItems}>
            <NavigationItem link="/check-out"><span className={classes.basket}>
                <span className={classes.totalItems}>{props.cartItems && props.cartItems.length > 0 ? props.cartItems.length : null}</span>
                <i className="fas fa-shopping-basket"></i></span></NavigationItem>
            <NavigationItem link="/" exact>Menu</NavigationItem>
            {!props.isAuthenticated
                ? null
                : <NavigationItem link="/logout">Logout</NavigationItem>}
        </ul>
    );
};
const mapStateToProps = state => { 
    return {
        cartItems: state.orders.cartItems
    };
};
const mapDispatchToProps = dispatch => {
    return {
        // addCategory: (category) => dispatch(actions.categoryToAdd(category))
    };
};
export default connect(mapStateToProps, mapDispatchToProps)(NavigationItems);