import React, {useState} from "react";
import {connect} from 'react-redux';
import * as actions from '../../store/actions/index';
// import classes from './NewCategory.module.css';
import globalCss from '../../global.module.css';
import { updateObject, checkValidity } from '../../shared/utility';
import Button from '../UI/Button/Button';
import Spinner from '../UI/Spinner/Spinner';

const  NewCategory = (props) => {
    const [categoryToAdd, setCategory] = useState({
        name:{
            config:{
                type: 'text',
                placeholder: 'Menu item name'
            },
            value: '',
            validation:{
                required: true,
                minLength: 2
            },
            valid: false,
            touched: false
        }
    });
    const inputChangedHandler = (event, controlName) => {
        const updatedControls = updateObject(categoryToAdd, {
          [controlName]: updateObject(categoryToAdd[controlName], {
            value: event.target.value,
            valid: checkValidity(
              event.target.value,
              categoryToAdd[controlName].validation
            ),
            touched: true
          })
        });
        setCategory(updatedControls);
      };
    const submitHandler = event => {
        event.preventDefault();
        props.addCategory(categoryToAdd.name.value);
    };
        let form = <form onSubmit={submitHandler}>
                    <label className={globalCss.Label}>
                        Category name:
                    </label>
                        <input className={
                            !categoryToAdd.name.valid && categoryToAdd.name.touched ?
                            [globalCss.InputElement, globalCss.Invalid].join(' ') : globalCss.InputElement }
                            value={categoryToAdd.name.value} 
                            onChange={event=>inputChangedHandler(event, 'name')} 
                        {...categoryToAdd.name.config} />
                    <Button btnType="Success">{props.loading ? '' : 'Submit'}</Button>
                </form>;
        if(props.loading){
            form = <Spinner />;
        }
        let errorMessage = null;
        if(props.error){
            errorMessage = (
                <p style={{color:'red'}}>{props.error}</p>
            );
        }
        return (
            <div className={globalCss.app_box}>
                <h4>Add New Category</h4>
                {errorMessage}
                {form}
            </div>
        );
};
const mapStateToProps = state => { 
    return {
        loading: state.items.loading,
        error: state.items.categoryError,
        isAuthenticated: state.auth.token !== null,
        authRedirectPath: state.auth.authRedirectPath
    };
};
const mapDispatchToProps = dispatch => {
    return {
        addCategory: (category) => dispatch(actions.categoryToAdd(category))
    };
};
export default connect(mapStateToProps, mapDispatchToProps)(NewCategory);