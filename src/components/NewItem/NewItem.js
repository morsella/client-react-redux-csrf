import React, {useState, useContext} from "react";
import {connect} from 'react-redux';
import * as actions from '../../store/actions/index';
// import classes from './NewItem.module.css';
import globalCss from '../../global.module.css';
import { updateObject, checkValidity } from '../../shared/utility';
import Button from '../UI/Button/Button';
import Spinner from '../UI/Spinner/Spinner';
import Dropdown from '../UI/Dropdown/Dropdown';
import DropdownContext from '../UI/Dropdown/dropdown-context';
const  NewItem = (props) => {
    const [itemToAdd, setItemToAdd] = useState({
        category: {
            value: {}
        },
        name:{
            config:{
                type: 'text',
                placeholder: 'Menu item name'
            },
            value: '',
            validation:{
                required: true,
                minLength: 2
            },
            valid: false,
            touched: false
        },
        description:{
            config:{
                type: 'textarea',
                placeholder: 'Description here',
                rows: 2,
                cols: 2
            },
            value: '',
            validation:{
                required: false
            },
            valid: true,
            touched: false
        },
        price1:{
            config:{
                type: 'text',
                placeholder: '00.00'
            },
            value: '',
            validation: {
                required: false,
                isDecimal: true
            },
            valid: false,
            touched: false
        },
        price2:{
            config:{
                type: 'text',
                placeholder: '00.00'
            },
            value: '',
            validation: {
                required: false,
                isDecimal: true
            },
            valid: false,
            touched: false
        }
    });
    const selected = useContext(DropdownContext);
    const inputChangedHandler = (event, controlName) => {
        const updatedControls = updateObject(itemToAdd, {
          [controlName]: updateObject(itemToAdd[controlName], {
            value: event.target.value,
            valid: checkValidity(
              event.target.value,
              itemToAdd[controlName].validation
            ),
            touched: true
          })
        });
        setItemToAdd(updatedControls);
      };
    const submitHandler = event => {
        event.preventDefault();
        if(selected.selected !== null && selected.selected.name.trim() !== ''){
            const updateCategory = {
                ...itemToAdd
            };
            updateCategory.category.value = selected.selected;
            setItemToAdd(updateCategory);
        }
        sendItemObj();
    };
    const sendItemObj = () =>{
        let item = {
            category: {
                id: itemToAdd.category.value.id,
                name: itemToAdd.category.value.name
            },
            name: itemToAdd.name.value,
            description: itemToAdd.description.value,
            price1: itemToAdd.price1.value,
            price2: itemToAdd.price2.value
        };
        props.addItem(item);
    };
        let form = <form onSubmit={submitHandler}>
                    <Dropdown
                        items={props.categories}
                        selectedItem={props.categories[0]}
                    />
                    {/* <select
                        // className={inputClasses.join(' ')}
                        value={itemToAdd.category.value}
                        onChange={event=>inputChangedHandler(event, 'category')}>
                        {itemToAdd.category.config.options.map(option => (
                            <option key={option.value} value={option.value}>
                                {option.displayValue}
                            </option>
                        ))}
                    </select> */}
                    <label className={globalCss.Label}>
                        Menu item name:
                    </label>
                        <input className={
                            !itemToAdd.name.valid && itemToAdd.name.touched ?
                            [globalCss.InputElement, globalCss.Invalid].join(' ') : globalCss.InputElement }
                            value={itemToAdd.name.value} 
                            onChange={event=>inputChangedHandler(event, 'name')} 
                        {...itemToAdd.name.config} />
                    <label className={globalCss.Label}>
                        Description:
                    </label>
                        <textarea className={!itemToAdd.description.valid && itemToAdd.description.touched ?
                            [globalCss.InputElement, globalCss.Invalid].join(' ') : globalCss.InputElement}
                            value={itemToAdd.description.value}
                        onChange={event => inputChangedHandler(event,'description')}  
                        {...itemToAdd.description.config}></textarea>
                    <label className={globalCss.Label}>
                        Price 1:
                    </label>
                        <input className={
                            !itemToAdd.price1.valid && itemToAdd.price1.touched ?
                            [globalCss.InputElement, globalCss.Invalid].join(' ') : globalCss.InputElement }
                            value={itemToAdd.price1.value} 
                            onChange={event=>inputChangedHandler(event, 'price1')} 
                        {...itemToAdd.price1.config} />
                    <label className={globalCss.Label}>
                        Price 2:
                    </label>
                        <input className={
                            !itemToAdd.price2.valid && itemToAdd.price2.touched ?
                            [globalCss.InputElement, globalCss.Invalid].join(' ') : globalCss.InputElement }
                            value={itemToAdd.price2.value} 
                            onChange={event=>inputChangedHandler(event, 'price2')} 
                        {...itemToAdd.price2.config} />
                    <Button btnType="Success">Submit</Button>
                </form>;
        if(props.loading){
            form = <Spinner />;
        }
        let errorMessage = null;
        if(props.error){
            errorMessage = (
                <p style={{color:'red'}}>{props.error}</p>
            );
        }
        let authRedirect = null;
        // if(props.isAuthenticated){
        //     authRedirect = <Redirect to={props.authRedirectPath} />
        // }
        return (
            <div className={globalCss.app_box}>
                <h4>Add New Menu Item</h4>
                {errorMessage}
                {authRedirect}
                {form}
            </div>
        );
};
const mapStateToProps = state => { 
    return {
        loading: state.items.loading,
        error: state.items.itemError,
        isAuthenticated: state.auth.token !== null,
        authRedirectPath: state.auth.authRedirectPath,
        categories: state.items.categories
    };
};
const mapDispatchToProps = dispatch => {
    return {
        addItem: (item) => dispatch(actions.itemToAdd(item))
    };
};
export default connect(mapStateToProps, mapDispatchToProps)(NewItem);