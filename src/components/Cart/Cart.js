import React, { useState} from 'react';
// import {Redirect} from 'react-router-dom';
import { connect } from 'react-redux';
import { Link } from 'react-router-dom';
import * as actions from '../../store/actions/index';
import Spinner from '../UI/Spinner/Spinner';
import classes from './Cart.module.css';
import { updateObject, checkValidity } from '../../shared/utility';
import globalCss from '../../global.module.css';


const Cart = props => {
    const [cartForm, setCartForm] = useState({
        remarks:{
            elementConfig: {
                rows: '5',
                cols: '20',
                placeholder: 'Your remarks'
            },
            value: '',
            validation:{
                required: false,
                isString: true
            },
            valid: false,
            touched: false
        }
    });
    let cartArr = <Spinner />;
    if(props.cart.length > 0){
        let allCartItems = [...props.cart];
       cartArr =  allCartItems.map((i, index)=> {
            return (
                <li className={classes.cart_item} key={index} onClick={()=> props.remove(i)}>
                        <div className={classes.cart_item_quantity}>{i.quantity}</div>
                        <div className={classes.cart__item_category}>{i.category}</div>
                        <span className={classes.cart_item_name}>{i.name}</span>
                        <span className={classes.cart_item_size}>{i.size}</span>
                        <div className={classes.cart_item_price}>{i.price1 === '0.00' ? null : i.price1 } {i.price2 === '0.00' ? null : i.price2 } €</div>
                </li>
            );
        });
        //if no items don't navigate
    }
    let errorMessage = null;
    const inputChangedHandler = (event, controlName) => {
        const updatedControls = updateObject(cartForm, {
          [controlName]: updateObject(cartForm[controlName], {
            value: event.target.value,
            valid: checkValidity(
              event.target.value,
              cartForm[controlName].validation
            ),
            touched: true
          })
        });
        setCartForm(updatedControls);
        console.log('updated', updatedControls.remarks.valid);
        if(updatedControls.remarks.valid){
            props.setCartRemarks(updatedControls.remarks.value);
        }
      };
        let totals = <div>
            calculate totals
        </div>;
        let form = <form>
                    <label className={globalCss.Label}>
                        Remarks:
                    </label>
                        <textarea className={!cartForm.remarks.valid && cartForm.remarks.touched ?
                            [globalCss.InputElement, globalCss.Invalid].join(' ') : globalCss.InputElement}
                             value={cartForm.remarks.value}
                        onChange={event => inputChangedHandler(event,'remarks')}  
                        {...cartForm.remarks.elementConfig}></textarea>
                </form>;

    if(props.page !== 'check-out'){
        return (
            <React.Fragment>
                <div>
                    <ul>                
                        <h4>Cart Items:</h4>
                        {cartArr}
                    </ul>
                <Link className={classes.order_btn_link}to={`/check-out`}><button className={classes.order_button} >Order</button></Link>
                </div>
            </React.Fragment>
        );
    }else{
        return (
            <React.Fragment>
                <div>
                    <ul>                
                        <h4>Cart Items:</h4>
                        {cartArr.length > 0 ? cartArr : 'not found'  }
                    </ul>
                    <div>
                    {errorMessage}
                    {totals}
                    {form}
                    </div>
                </div>
            </React.Fragment>
        );
    }

};

const mapStateToProps = state => {
    return {
        cart: state.orders.cartItems,
        loading: state.auth.loading,
        error: state.auth.error
    };
};
const mapDispatchToProps = dispatch =>{
    return {
        remove: (item) => dispatch(actions.removeCartItem(item)),
        setCartRemarks: (remarks) => dispatch(actions.cartRemarks(remarks))
        // onLogout: () => dispatch(actions.removeAuth())
    };
};

export default connect (mapStateToProps, mapDispatchToProps)(Cart);