import React from 'react';

const dropDownContext = React.createContext({ selected: null, setDropdownSelected: (item)=>{} });

export default dropDownContext;