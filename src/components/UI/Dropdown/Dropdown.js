import React, {useState, useEffect, useContext} from 'react';
import  classes from './Dropdown.module.css';
import DropdownContext from './dropdown-context';


const Dropdown = (props) => {
    const [showItems, setShowItems] = useState(false);
    const [itemSelected, setSelectItem] = useState({});
    const [items, setItems] = useState([]);
    const selected = useContext(DropdownContext);

    useEffect(()=>{
        let arr = props.items.filter(i => i.id !== props.selectedItem.id);
        setItems(arr);
        setSelectItem(props.selectedItem);
        selected.setDropdownSelected(props.selectedItem);
    // eslint-disable-next-line react-hooks/exhaustive-deps
    },[]);
    const dropDown = () => {
        setShowItems(!showItems);
    };

    const selectItem = (item) => {
        let arr = props.items.filter(i => i.id !== item.id);
        setItems(arr);
        setSelectItem(item);
        setShowItems(false);
        selected.setDropdownSelected(item);
    };

    return (
        <React.Fragment>
            <div className={classes.dropdown_box}>
                <div className={classes.dropdown_header} onClick={dropDown}>
                    <span className={classes.dropDown_selected}>{ itemSelected.name }</span>
                    {!showItems ? <span className={classes.arrow_icon_down}><i className="fas fa-sort-down"></i></span> 
                    : <span className={classes.arrow_icon_up}><i className="fas fa-sort-up"></i></span> }
                </div>
                <div
                className={classes.dropdown_items}
                style={{display: showItems ? 'block' : 'none'}}>
                {
                    items.map(item => <div
                    className={classes.dropdown_item}
                    key={item.id}
                    onClick={() => selectItem(item)}>
                    { item.name }
                    </div>)
                }
                </div>
            </div>
        </React.Fragment>
    );
};

export default Dropdown;