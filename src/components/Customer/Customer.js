import React, { useState } from 'react';
import { connect } from 'react-redux';
import { Redirect } from 'react-router-dom';
// import Button from '../../components/UI/Button/Button';
import Spinner from '../../components/UI/Spinner/Spinner';
import classes from './Customer.module.css';
import * as actions from '../../store/actions/index';
import { updateObject, checkValidity } from '../../shared/utility';
import globalCss from '../../global.module.css';
import Cart from '../Cart/Cart';
const Customer = props => {
    const [customerForm, setCustomerForm] = useState({
        firstName:{
            elementConfig: {
                type: 'text',
                placeholder: 'Your first name'
            },
            value: '',
            validation:{
                required: true,
                isString: true
            },
            valid: false,
            touched: false
        },
        lastName:{
            elementConfig: {
                type: 'text',
                placeholder: 'Your last name'
            },
            value: '',
            validation:{
                required: true,
                isString: true
            },
            valid: false,
            touched: false
        },
        companyName:{
            elementConfig: {
                type: 'text',
                placeholder: 'Company name'
            },
            value: '',
            validation:{
                required: true,
                isString: true
            },
            valid: false,
            touched: false
        },
        street:{
            elementConfig: {
                type: 'text',
                placeholder: 'Street for delivery'
            },
            value: '',
            validation:{
                required: true,
                isString: true
            },
            valid: false,
            touched: false
        },
        houseNumber:{
            elementConfig: {
                type: 'text',
                placeholder: 'House number'
            },
            value: '',
            validation:{
                required: true,
                isString: true
            },
            valid: false,
            touched: false
        },
        afix:{
            elementConfig: {
                type: 'text',
                placeholder: 'Afix'
            },
            value: '',
            validation:{
                required: true,
                isString: true
            },
            valid: false,
            touched: false
        },
        postCode:{
            elementConfig: {
                type: 'text',
                placeholder: 'Postcode'
            },
            value: '',
            validation:{
                required: true,
                isString: true
            },
            valid: false,
            touched: false
        },
        city:{
            elementConfig: {
                type: 'text',
                placeholder: 'City'
            },
            value: '',
            validation:{
                required: true,
                isString: true
            },
            valid: false,
            touched: false
        },
        phoneNumber:{
            elementConfig: {
                type: 'tel',
                placeholder: 'Your phone number'
            },
            value: '',
            validation:{
                required: true,
                isNumeric: true
            },
            valid: false,
            touched: false
        },
        email:{
            elementConfig: {
                type: 'email',
                placeholder: 'Your email address'
            },
            value: '',
            validation:{
                required: true,
                isEmail: true
            },
            valid: false,
            touched: false
        }
    });
    const inputChangedHandler = (event, controlName) => {
        const updatedControls = updateObject(customerForm, {
          [controlName]: updateObject(customerForm[controlName], {
            value: event.target.value,
            valid: checkValidity(
              event.target.value,
              customerForm[controlName].validation
            ),
            touched: true
          })
        });
        setCustomerForm(updatedControls);
        if(updatedControls[controlName].valid){
            props.setCustomerForm(updatedControls[controlName].value, controlName);
        }
      };
        let form = <form>
                    <label className={globalCss.Label}>
                        First name:
                    </label>
                        <input className={
                            !customerForm.firstName.valid && customerForm.firstName.touched ?
                            [globalCss.InputElement, globalCss.Invalid].join(' ') : globalCss.InputElement }
                            value={customerForm.firstName.value} 
                        onChange={event=>inputChangedHandler(event, 'firstName')} 
                        {...customerForm.firstName.elementConfig} />
{/* ************************************************************************************************ */}
                    <label className={globalCss.Label}>
                        Last name:
                    </label>
                        <input className={
                            !customerForm.lastName.valid && customerForm.lastName.touched ?
                            [globalCss.InputElement, globalCss.Invalid].join(' ') : globalCss.InputElement }
                            value={customerForm.lastName.value} 
                        onChange={event=>inputChangedHandler(event, 'lastName')} 
                        {...customerForm.lastName.elementConfig} />
{/* ************************************************************************************************ */}
                    <label className={globalCss.Label}>
                        Company name:
                    </label>
                        <input className={
                            !customerForm.companyName.valid && customerForm.companyName.touched ?
                            [globalCss.InputElement, globalCss.Invalid].join(' ') : globalCss.InputElement }
                            value={customerForm.companyName.value} 
                        onChange={event=>inputChangedHandler(event, 'companyName')} 
                        {...customerForm.companyName.elementConfig} />
{/* ************************************************************************************************ */}
                    <label className={globalCss.Label}>
                        Street:
                    </label>
                        <input className={
                            !customerForm.street.valid && customerForm.street.touched ?
                            [globalCss.InputElement, globalCss.Invalid].join(' ') : globalCss.InputElement }
                            value={customerForm.street.value} 
                        onChange={event=>inputChangedHandler(event, 'street')} 
                        {...customerForm.street.elementConfig} />
{/* ************************************************************************************************ */}
                    <label className={globalCss.Label}>
                        House number:
                    </label>
                        <input className={
                            !customerForm.houseNumber.valid && customerForm.houseNumber.touched ?
                            [globalCss.InputElement, globalCss.Invalid].join(' ') : globalCss.InputElement }
                            value={customerForm.houseNumber.value} 
                        onChange={event=>inputChangedHandler(event, 'houseNumber')} 
                        {...customerForm.houseNumber.elementConfig} />
{/* ************************************************************************************************ */}
                    <label className={globalCss.Label}>
                        Afix:
                    </label>
                        <input className={
                            !customerForm.afix.valid && customerForm.email.touched ?
                            [globalCss.InputElement, globalCss.Invalid].join(' ') : globalCss.InputElement }
                            value={customerForm.afix.value} 
                        onChange={event=>inputChangedHandler(event, 'afix')} 
                        {...customerForm.afix.elementConfig} />
{/* ************************************************************************************************ */}
                    <label className={globalCss.Label}>
                        Postcode:
                    </label>
                        <input className={
                            !customerForm.postCode.valid && customerForm.email.touched ?
                            [globalCss.InputElement, globalCss.Invalid].join(' ') : globalCss.InputElement }
                            value={customerForm.postCode.value} 
                        onChange={event=>inputChangedHandler(event, 'postCode')} 
                        {...customerForm.postCode.elementConfig} />
{/* ************************************************************************************************ */}
                    <label className={globalCss.Label}>
                        City:
                    </label>
                        <input className={
                            !customerForm.city.valid && customerForm.email.touched ?
                            [globalCss.InputElement, globalCss.Invalid].join(' ') : globalCss.InputElement }
                            value={customerForm.city.value} 
                        onChange={event=>inputChangedHandler(event, 'city')} 
                        {...customerForm.city.elementConfig} />
{/* ************************************************************************************************ */}
                    <label className={globalCss.Label}>
                        Phone number:
                    </label>
                        <input className={
                            !customerForm.phoneNumber.valid && customerForm.email.touched ?
                            [globalCss.InputElement, globalCss.Invalid].join(' ') : globalCss.InputElement }
                            value={customerForm.phoneNumber.value} 
                        onChange={event=>inputChangedHandler(event, 'phoneNumber')} 
                        {...customerForm.phoneNumber.elementConfig} />
{/* ************************************************************************************************ */}
                    <label className={globalCss.Label}>
                        Email:
                    </label>
                        <input className={
                            !customerForm.email.valid && customerForm.email.touched ?
                            [globalCss.InputElement, globalCss.Invalid].join(' ') : globalCss.InputElement }
                            value={customerForm.email.value} 
                        onChange={event=>inputChangedHandler(event, 'email')} 
                        {...customerForm.email.elementConfig} />
{/* ************************************************************************************************ */}
                </form>;
        if(props.loading){
            form = <Spinner />;
        }
        let errorMessage = null;
        if(props.error){
            errorMessage = (
                <p>{props.error}</p>
            );
        }
        let authRedirect = null;
        if(props.isAuthenticated){
            authRedirect = <Redirect to={props.authRedirectPath} />
        }
        return (
            <React.Fragment>
                <div className={classes.app_box_container}>
                <div className={classes.app_box}>
                    <Cart page={'check-out'} />
                </div>
                    <div className={classes.app_box}>
                        <h4>Customer and Delivery Details:</h4>
                        {errorMessage}
                        {authRedirect}
                        {form}
                    </div>
                </div>
            </React.Fragment>
          );
};
const mapStateToProps = state => { 
    return {
        loading: state.auth.loading,
        error: state.auth.error,
        isAuthenticated: state.auth.token !== null,
        authRedirectPath: state.auth.authRedirectPath,
        csrf: state.session.csrf
    };
};
const mapDispatchToProps = dispatch => {
    return {
        setCustomerForm: (customerFormValue, controlName) => dispatch(actions.customerForm(customerFormValue, controlName))
        // onAuth: (email, password, csrf) => dispatch(actions.auth(email, password, csrf))
    };
};
export default connect(mapStateToProps, mapDispatchToProps)(Customer);