import React from 'react';
import { ReactComponent as Logo } from '../../assets/images/logo_v1.svg';
import classes from '../Logo/Logo.module.css';
const logo = (props) => (
    <div className={classes.Logo} style={{height: props.height, width: props.width}}>
        <Logo />
    </div>
);

export default logo;