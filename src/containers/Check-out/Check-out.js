import React, { useEffect} from 'react';
// import {Redirect} from 'react-router-dom';
import { connect } from 'react-redux';
import * as actions from '../../store/actions/index';
// import Spinner from '../../components/UI/Spinner/Spinner';
import classes from './Check-out.module.css';
import Customer from '../../components/Customer/Customer';
const CheckOut  = props => {
    console.log('customer check-out page', props.customer);
    useEffect(()=>{
        props.fetchMethods();
    // eslint-disable-next-line react-hooks/exhaustive-deps
    },[]);
    const toPay = () =>{
        const payment = {
            _csrf: props.csrf,
            method: 'ideal',
            amount: {
                value:    '0.01',
                currency: 'EUR'
              }
        }
        props.createPayment(payment);
    }
    const methodsArr = props.methods.map(m => {
        return (
        <ul className={classes.menu_parent_items} key={m.id}>
            <li className={classes.menu_items}><p>{m.description}</p>
                <img src={m.image.svg} alt={m.id}/>
            </li>
        </ul>
      )});
    return (
        <React.Fragment>
            <Customer />
            <div className={classes.check_cart_list}>{methodsArr}</div>
            <button onClick={() => toPay()}>Pay</button>
        </React.Fragment>
    )
};
const mapStateToProps = state => {
    return {
        methods: state.orders.methods,
        csrf: state.session.csrf,
        customer: state.orders.customerForm
    };
};
const mapDispatchToProps = dispatch =>{
    return {
        fetchMethods: () => dispatch(actions.fetchMethods()),
        getCartItems: () => dispatch(actions.getCartItems()),
        createPayment: (payment) => dispatch(actions.createPayment(payment))
    };
};
export default connect (mapStateToProps, mapDispatchToProps)(CheckOut);