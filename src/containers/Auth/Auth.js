import React, { useState } from 'react';
import { connect } from 'react-redux';
import { Redirect } from 'react-router-dom';
import Button from '../../components/UI/Button/Button';
import Spinner from '../../components/UI/Spinner/Spinner';
import globalCss from '../../global.module.css';
import * as actions from '../../store/actions/index';
import { updateObject, checkValidity } from '../../shared/utility';

const Auth = props => {
    const [authForm, setAuthForm] = useState({
        email:{
            elementConfig: {
                type: 'email',
                placeholder: 'Your email address'
            },
            value: '',
            validation:{
                required: true,
                isEmail: true
            },
            valid: false,
            touched: false
        },
        password:{
            elementConfig: {
                type: 'password',
                placeholder: 'Password'
            },
            value: '',
            validation:{
                required: true,
                minLength: 8,
                isPassword: true
            },
            valid: false,
            touched: false
        }
    });
    const inputChangedHandler = (event, controlName) => {
        const updatedControls = updateObject(authForm, {
          [controlName]: updateObject(authForm[controlName], {
            value: event.target.value,
            valid: checkValidity(
              event.target.value,
              authForm[controlName].validation
            ),
            touched: true
          })
        });
        setAuthForm(updatedControls);
      };
    const submitHandler = event => {
        event.preventDefault();
        //On Submit get value from form and validate
        props.onAuth(authForm.email.value, authForm.password.value, props.csrf);
    };
        let form = <form onSubmit={submitHandler}>
                    <label className={globalCss.Label}>
                        Email:
                    </label>
                        <input className={
                            !authForm.email.valid && authForm.email.touched ?
                            [globalCss.InputElement, globalCss.Invalid].join(' ') : globalCss.InputElement }
                            value={authForm.email.value} 
                        onChange={event=>inputChangedHandler(event, 'email')} 
                        {...authForm.email.elementConfig} />
                    <label className={globalCss.Label}>
                        Password:
                    </label>
                        <input className={!authForm.password.valid && authForm.password.touched ?
                            [globalCss.InputElement, globalCss.Invalid].join(' ') : globalCss.InputElement}
                             value={authForm.password.value}
                        onChange={event => inputChangedHandler(event,'password')}  
                        {...authForm.password.elementConfig} />
                    <Button btnType="Success">Submit</Button>
                </form>;
        if(props.loading){
            form = <Spinner />;
        }
        let errorMessage = null;
        if(props.error){
            errorMessage = (
                <p>{props.error}</p>
            );
        }
        let authRedirect = null;
        if(props.isAuthenticated){
            authRedirect = <Redirect to={props.authRedirectPath} />
        }
        return (
            <div className={globalCss.app_box}>
                {errorMessage}
                {authRedirect}
                {form}
            </div>
          );
};
const mapStateToProps = state => { 
    return {
        loading: state.auth.loading,
        error: state.auth.error,
        isAuthenticated: state.auth.token !== null,
        authRedirectPath: state.auth.authRedirectPath,
        csrf: state.session.csrf
    };
};
const mapDispatchToProps = dispatch => {
    return {
        onAuth: (email, password, csrf) => dispatch(actions.auth(email, password, csrf))
    };
};
export default connect(mapStateToProps, mapDispatchToProps)(Auth);