import React from 'react';
import Aux from '../../hoc/Aux';
import {connect} from 'react-redux';
import classes from './Layout.module.css';
import Toolbar from '../../components/Navigation/Toolbar/Toolbar';
// import SideDrawer from '../../components/Navigation/SideDrawer/SideDrawer';
const Layout = props => {
    // const [sideDrawerIsVisible, setSideDrawerVisible] = useState(false);
    // const sideDrawerClosedHandler = () =>{
    //     setSideDrawerVisible(false);
    // };
    // const sideDrawerToggleHandler = () =>{
    //     setSideDrawerVisible(!sideDrawerIsVisible);
    // };
        return (
            <Aux>
                <Toolbar isAuth={props.isAuthenticated}/>
                {/* <SideDrawer isAuth={props.isAuthenticated}
                 open={sideDrawerIsVisible} closed={sideDrawerClosedHandler}/> */}
                <main className={classes.content}>
                    {props.children}
                </main>
            </Aux>
        );
}
const mapStateToProps = state => {
    return {
        isAuthenticated: state.auth.token !== null
    };
};
export default connect(mapStateToProps)(Layout);