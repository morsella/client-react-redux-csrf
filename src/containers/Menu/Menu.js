import React, {useState, useEffect} from 'react';
import {connect} from 'react-redux';
import * as actions from '../../store/actions/index';
import classes from './Menu.module.css';
import NewItem from '../../components/NewItem/NewItem';
import NewCategory from '../../components/NewCategory/NewCategory';
import Button from '../../components/UI/Button/Button';
import Spinner from '../../components/UI/Spinner/Spinner';
import Logo from '../../components/Logo/Logo';
import Cart from '../../components/Cart/Cart';
const Menu = props => {
    const [showAddNew, setShowAddNew] = useState(false);
    useEffect(()=>{
        props.getCategories();
        props.getItems();
    // eslint-disable-next-line react-hooks/exhaustive-deps
    }, []);
    const addCartItem = (item, price) => {
        let itemToAdd = {...item};
        if(price === 1){
            itemToAdd.price2 = null;
            itemToAdd.size = 'Small';
        }else{
            itemToAdd.price1 = null;
            itemToAdd.size = 'Large';
        }
        props.addCartItem(itemToAdd, props.csrf);
    };

    // If logged in show add new items
    let menuArr = <Spinner />;
    if (!props.loading && !props.sessionLoading) {
        let allCategories = [...props.categories];
        let allItems = [...props.items];
        allCategories.forEach((c)=>{
            let filterItems = allItems.filter((i)=> {
                if(c.id === i.categoryId) i.category = c.name;
                return i.categoryId === c.id;
            });
            c.items = filterItems;
        });
        menuArr = allCategories.map(cat => (
        <li className={classes.menu_category} key={cat.id}><div><h2 >{cat.name}</h2>
        <small>small</small><span>|</span><small>big</small></div><ul className={classes.menu_parent_items}>{
            cat.items.map((i)=>(
            <li className={classes.menu_items} key={i.id}><h4>{i.name}</h4> <p>{i.description}</p>
            <small onClick={() => addCartItem(i, 1)}>{i.price1}</small>{i.price2 === '0.00' ? null : <small onClick={() => addCartItem(i, 2)}>{i.price2}</small>}<span>€</span>
            </li>
            ))
        }</ul></li> 
      ));
    }
    const showNew = () =>{
        setShowAddNew(!showAddNew);
    };
    if(props.isAuthenticated){
        return (
            <React.Fragment>
                <div className={showAddNew ? [classes.add_new_button, classes.close].join(' ') : [classes.add_new_button, classes.open].join(' ')} onClick={showNew}>
                {showAddNew ? <Button >Close</Button> : <Button>Add New</Button>} 
                </div>
                { showAddNew ? <div><NewCategory /><NewItem categories={props.categories} /></div> : null }
                <div className={classes.menu_list}><div className={classes.menu_list_bg}><Logo className={classes.logo} /></div><ul className={classes.menu_parent_category}>{menuArr}</ul></div>
            </React.Fragment>
        );
    }else{
        return (
            <React.Fragment>
            <div className={classes.menu_container}>
                <div className={classes.menu_sale}>Sale</div>
                <div className={classes.menu_list}><div className={classes.menu_list_bg}><Logo  className={classes.logo}/></div><ul className={classes.menu_parent_category}>{menuArr}</ul></div>
                {props.cartItems.length > 0 ? <div className={classes.menu_cart}><Cart /></div>: null}
            </div>
            </React.Fragment>
        );
    }
};
const mapStateToProps = state => {
    return {
        loading: state.items.loading,
        sessionLoading: state.session.loading,
        error: state.items.categoryError,
        categories: state.items.categories,
        items: state.items.items,
        isAuthenticated: state.auth.token !== null,
        cartItems: state.orders.cartItems,
        csrf: state.session.csrf,
        cartLoading: state.orders.loading
    };
};
const mapDispatchToProps = dispatch =>{
    return{
        getCategories: () => dispatch(actions.fetchCategories()),
        getItems: () => dispatch(actions.fetchItems()),
        addCartItem: (item, csrf) => dispatch(actions.cartInit(item, csrf))
    };
};

export default connect(mapStateToProps, mapDispatchToProps)(Menu);