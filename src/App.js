import React, { useEffect, Suspense, useState } from 'react';
import {Route, Switch, withRouter, Redirect} from 'react-router-dom';
import { connect } from 'react-redux';
import './App.css';

import * as actions from './store/actions/index';
import Layout from './containers/Layout/Layout';
import Menu from './containers/Menu/Menu';
import Logout from './containers/Auth/Logout/Logout';
import DropdownContext from './components/UI/Dropdown/dropdown-context';

// const Orders = React.lazy(() => {
//   return import('./containers/Orders/Orders');
// });

const Auth = React.lazy(() => {
  return import('./containers/Auth/Auth');
});
const CheckOut = React.lazy(()=>{
  return import('./containers/Check-out/Check-out');
});
const App = props => {
  const [dropDownSelected, setDropdown] = useState(null);

  useEffect(() => {
    props.onTryAuthLogin();
    props.getSession();
    //sessionSuccess => Loading spinner when session is created. 
    // only then the items can be fetched.
    if(!props.sessionSuccess){
      props.getCartItems();
  }
  // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);
  const setDropdownSelected = (item)=>{
    setDropdown(item);
  };
    let routes = (
      <Switch>
        <Route path="/check-out" render={ props => <CheckOut {...props} />} />
        <Route path="/admin" render={props => <Auth {...props} />} />
        <Route path="/" exact component={Menu} />
        <Redirect to="/"/>
      </Switch>
    );
    if(props.isAuthenticated){
      routes=(
        <Switch>
          <Route path="/orders" component={Menu} />
          <Route path="/check-out" render={props => <CheckOut {...props} />} />
          <Route path="/logout" component={Logout} />
          <Route path="/" exact component={Menu} />
          <Redirect to="/" />
        </Switch>
      );
    }

    return (
      <div>
        <DropdownContext.Provider value={{ selected: dropDownSelected, setDropdownSelected: setDropdownSelected }}>
          <Layout><Suspense fallback={<p>Loading...</p>}>{routes}</Suspense></Layout>
        </DropdownContext.Provider>
      </div>
    );
};
const mapStateToProps = state => {
  return {
    isAuthenticated: state.auth.token !== null,
    sessionSuccess: state.session.loading
  };
};
const mapDispatchToProps = dispatch =>{
  return {
    onTryAuthLogin: () => dispatch(actions.authCheckState()),
    getSession: () => dispatch(actions.getSession()),
    getCartItems: () => dispatch(actions.getCartItems())
  };
};
export default withRouter(connect(mapStateToProps, mapDispatchToProps)(App));
