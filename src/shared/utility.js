
export const updateObject = (oldObject, updatedProperties) => {
    return {
        ...oldObject,
        ...updatedProperties
    };
};

export const checkValidity = ( value, rules ) => {
    let isValid = true;
    if ( !rules ) {
        return true;
    }
    //if required and not empty
    if (rules.required) {
        isValid = value.trim() !== '' && isValid;
    }
    
    if (rules.isString) {
        isValid = ((typeof value === 'string' || value instanceof String) && isValid) ? true : false;
    }
    if ( rules.minLength ) {
        isValid = value.length >= rules.minLength && isValid
    }

    if ( rules.maxLength ) {
        isValid = value.length <= rules.maxLength && isValid
    }

    if ( rules.isEmail ) {
        const pattern = /[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?/;
        isValid = pattern.test( value ) && isValid
    }

    if ( rules.isNumeric ) {
        const pattern = /^\d+$/;
        isValid = pattern.test( value ) && isValid
    }
    if (rules.isPassword) {
        const pattern= /^(?=[^A-Z]*[A-Z])(?=[^a-z]*[a-z])(?=[^0-9]*[0-9]).{8,}$/;
        isValid = pattern.test(value) && isValid
    }
    if ( rules.isDecimal && value.trim() !== '' ){
        const pattern = /^\s*(?=.*[1-9])\d*(?:\.\d{1,2})?\s*$/;
        isValid = pattern.test(value) && isValid
    }
    console.log(isValid);
    return isValid;
};